// Цвет отключенной тблицы
let disabledTableColor = '#F84825'
// Цвет при наведении на таблицу
let hoverColor = '#75a6d1'

let allTables = {}
let disabledTables = []

let isPanning = false

window.addEventListener('load', () => {
    let container = document.querySelector('#svg')
    let content = container.contentDocument.querySelector('svg')

    content.addEventListener('wheel', (e) => {e.preventDefault()}, { passive: false })

    svgPanZoom(content, {
        controlIconsEnabled: true,
        dblClickZoomEnabled: false,
        onPan: onPan,
        onUpdatedCTM: onPanEnd
    })

    init(content)
})

const onPan = () => {
    if (!isPanning) isPanning = true
}

const onPanEnd = () => {
    // Ждём 500мс, чтобы избежать клика по таблице после перемещения
    setTimeout(() => {
        if (isPanning) isPanning = false
    }, 500)
}

const init = (content) => {
    let tables = content.querySelectorAll('.table')

    // Добавляем обработчик кликов на каждую таблицу 
    tables.forEach(table => {
        let name = table.children[1].children[0].innerHTML
        let rect = table.children[2].children[0]
        let polyline = table.children[2]

        allTables[name] = {
            origin_color: rect.getAttribute('fill'),
            current_color: rect.getAttribute('fill')
        }

        // Проверяем есть ли таблица среди отключенных
        if (disabledTables.includes(name)) {
            rect.setAttribute('fill', disabledTableColor)
        }

        table.addEventListener('click', () => {
            if (isPanning) return

            let tableIndex = disabledTables.indexOf(name)
            if (tableIndex != -1) {
                rect.setAttribute('fill', allTables[name]["origin_color"])
                allTables[name]["current_color"] = allTables[name]["origin_color"]
                disabledTables.splice(tableIndex, 1)
            } else {
                rect.setAttribute('fill', disabledTableColor)
                allTables[name]["current_color"] = disabledTableColor
                disabledTables.push(name)
            }
        })

        table.addEventListener('mouseover', () => {
            rect.setAttribute('fill', hoverColor)
        })

        table.addEventListener('mouseleave', () => {
            rect.setAttribute('fill', allTables[name]["current_color"])
        })
    });
}